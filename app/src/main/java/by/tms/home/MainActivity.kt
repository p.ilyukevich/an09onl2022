package by.tms.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.tms.hw2.hw2

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        hw1()
        hw2()
    }

    private fun hw1() {
        println(checkCoordinates(-5, 0))
        println(checkCoordinates(0, 0))
        println(checkCoordinates(1, 1))
        println(checkCoordinates(-4, -3))
        println(checkCoordinates(2, 4))
    }

    private fun checkCoordinates(x: Int, y: Int) = when {
        x in -4..4 && y in -3..0 -> true
        x in -2..2 && y in 0..4 -> true
        else -> false
    }
}