package by.tms.hw3


fun main() {

    val country = Country(
        name = "Беларусь",
        regions = listOf(
            Region(
                "Минская обл", listOf(
                    City(
                        name = "Минск", isCapital = true, districts = listOf(
                            District("Советский"),
                            District("Первомайский"),
                            District("Партизанский"),
                            District("Заводской"),
                            District("Ленинский"),
                            District("Центральный"),
                            District("Октябрьский"),
                            District("Фрунзенский"),
                            District("Московский")
                        )
                    ),
                    City(name = "Заславль"),
                    City(name = "Молодечно")
                ), 40.2
            ),
            Region(
                "Гомельская обл", listOf(
                    City(
                        name = "Гомель", isRegionCenter = true, districts = listOf(
                            District("Центральный"),
                            District("Советский"),
                            District("Железнодорожный"),
                            District("Новобелицкий")
                        )
                    )
                ), 40.4
            ),
            Region(
                "Витебская обл", listOf(
                    City(name = "Витебск", isRegionCenter = true)
                ), 40.0
            ),
            Region("Брестская обл", listOf(), 32.8),
            Region("Гродненская обл", listOf(), 25.1),
            Region("Могилевская обл", listOf(), 29.1)
        )
    )
    country.run {
        println("Страна: $name")
        println("Столица: ${getCapital()}")
        println("Количество областей: ${getRegionCount()}")
        println("Площади областей: ${getRegionSquares()}")
        println("Областные центры: ${getRegionCenters()}")
    }
}