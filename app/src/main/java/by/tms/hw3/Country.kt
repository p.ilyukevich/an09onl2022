package by.tms.hw3

class Country(
    name: String,
    private val regions: List<Region>
) : Base(name) {

    fun getRegionCount(): Int {
        return regions.size
    }

    fun getCapital(): String? {
        return regions.map { it.findCapitalCity() }.firstOrNull()
    }

    fun getRegionSquares(): String {
        return regions.map {
            "${it.name} - ${it.square}"
        }.joinToString()

    }

    fun getRegionCenters(): String {
        return regions.map { it.findRegionCenters() }
            .filter { it.isNotEmpty() }
            .joinToString { it.first() }
    }
}

