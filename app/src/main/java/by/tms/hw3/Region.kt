package by.tms.hw3

//область
class Region(
    name: String,
    private val cities: List<City>,
    val square: Double
) : Base(name) {

    fun findCapitalCity(): String? {
        return cities.find { it.isCapital }?.name
    }

    fun findRegionCenters(): List<String> {
        return cities.filter { it.isRegionCenter }.map { it.name }
    }
}