package by.tms.hw3

class City(
    name: String,
    val isCapital: Boolean = false,
    val isRegionCenter: Boolean = false,
    private val districts: List<District> = listOf()
) : Base(name)