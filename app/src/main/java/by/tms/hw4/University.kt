package by.tms.hw4

import by.tms.hw4.Type.*
import java.util.*
import java.util.Collections.emptyList
import java.util.Comparator.*

class University(
    private val name: String,
    private val courses: List<Course>
) {

    fun getAllStudents(type: Type, name: String) =
        when (type) {
            GROUP -> {
                courses.flatMap { it.groups }
                    .filter { it.number == name }
                    .flatMap { it.students }
                    .toList()
            }
            COURSE -> {
                courses.filter { it.name == name }
                    .flatMap { it.groups }
                    .flatMap { it.students }
                    .toList()
            }
            UNIVERSITY -> if (this.name == name) getAllStudents() else emptyList()
        }

    private fun getAllStudents() =
        courses.flatMap { it.groups }
            .flatMap { it.students }
            .toList()

    fun getAverageAgeOfStudents() =
        courses.flatMap { it.groups }
            .flatMap { it.students }
            .map { it.age.toDouble() }
            .average()

    fun findYoungestStudent() =
        courses.flatMap { it.groups }
            .flatMap { it.students }
            .minWithOrNull(comparingInt { it.age })

    fun findOldestStudent() =
        courses.flatMap { it.groups }
            .flatMap { it.students }
            .maxWithOrNull(comparingInt { it.age })

    fun getCountOfStudents(averageMark: Double) =
        courses.flatMap { it.groups }
            .flatMap { it.students }
            .map { it.averageMark }
            .count { it > averageMark }
}