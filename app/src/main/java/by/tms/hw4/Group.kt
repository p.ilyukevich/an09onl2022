package by.tms.hw4

class Group(
    val number: String,
    val students: List<Student>
)