package by.tms.hw4

class Course(
    val name: String,
    val groups: List<Group>
)