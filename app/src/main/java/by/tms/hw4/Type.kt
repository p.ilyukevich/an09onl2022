package by.tms.hw4

enum class Type {
    GROUP, COURSE, UNIVERSITY
}