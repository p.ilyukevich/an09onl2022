package by.tms.hw4

import by.tms.hw4.Type.GROUP
import by.tms.hw4.Type.UNIVERSITY

fun main() {
    val university = University(
        "БГУ",
        listOf(
            Course(
                "1",
                listOf(
                    Group(
                        "A45", listOf(
                            Student("Ivan", "Ivan", 19, 4.5),
                            Student("Nik", "Nik", 19, 7.5),
                            Student("Petr", "Petr", 19, 9.5)
                        )
                    )
                )
            ),
            Course(
                "2",
                listOf(
                    Group(
                        "A35", listOf(
                            Student("IvanA35", "Ivan", 19, 3.5),
                            Student("NikA35", "Nik", 19, 6.3),
                            Student("PetrA35", "Petr", 19, 6.6)
                        )
                    )
                )
            ),
            Course(
                "3",
                listOf(
                    Group(
                        "A25", listOf(
                            Student("IvanA25", "Ivan", 19, 8.5),
                            Student("NikA25", "Nik", 19, 5.5),
                            Student("PetrA25", "Petr", 19, 3.5)
                        )
                    )
                )
            ),
        )
    )
    university.apply {
        getAllStudents(GROUP, "A25").print()
        getAllStudents(UNIVERSITY, "").print()

        println(getCountOfStudents(9.0))
        println(getAverageAgeOfStudents())

        findYoungestStudent().print()
        findOldestStudent().print()
    }
}

private fun List<Student>.print() {
    if (this.isNotEmpty()) {
        this.forEach { it.print() }
    } else {
        println("Не нашли студентов")
    }
}

private fun Student.print() {
    println("Имя: $firsName, Фамилия: $lastName, Возраст: $age")
}