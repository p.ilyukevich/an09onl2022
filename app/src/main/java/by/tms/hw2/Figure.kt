package by.tms.hw2

abstract class Figure : Shape {

    final override fun getName(): String {
        return this::class.simpleName!!
    }
}