package by.tms.hw2


interface Shape {
    fun getName(): String
    fun getSquare(): Double
    fun getPerimeter(): Double
}