package by.tms.hw2

import kotlin.math.PI
import kotlin.math.pow

class Circle(
    private val radius: Double,
) : Figure() {

    override fun getSquare(): Double {
        return PI * radius.pow(2)
    }

    override fun getPerimeter(): Double {
        return 2 * PI * radius
    }
}