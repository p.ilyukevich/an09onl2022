package by.tms.hw2

fun hw2() {
    val triangle = Triangle(3.0, 3.0, 4.0)
    val rectangle = Rectangle(4.0, 5.0)
    val circle = Circle(10.0)
    val figures = listOf(triangle, rectangle, circle)
    figures.forEach {
        it.print()
    }
}

fun Shape.print() {
    println("Имя: ${getName()}, Площадь: ${getSquare()}, Периметр: ${getPerimeter()}")
}
