package by.tms.hw2


class Rectangle(
    private val a: Double,
    private val b: Double,
) : Figure() {

    override fun getSquare(): Double {
        return a * b
    }

    override fun getPerimeter(): Double {
        return 2 * (a + b)
    }
}