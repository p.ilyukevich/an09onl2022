package by.tms.hw2


class Triangle(
    private val a: Double,
    private val b: Double,
    private val c: Double,
) : Figure() {

    private val p = getPerimeter() / 2

    override fun getSquare(): Double {
        return p * calc(a) * calc(b) * calc(c)
    }

    private fun calc(value: Double): Double {
        return p - value
    }

    override fun getPerimeter(): Double {
        return a + b + c
    }
}